package com.feng.cloud.controller;

import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.feng.springcloud.entities.CommonResult;

/**
 * @author JiangLifeng
 * @description
 * @createTime 2021年08月08日
 */
public class CustomerBlockHandler {
    public static CommonResult handlerException(BlockException exception) {
        return new CommonResult(4444,"按客戶自定义,global handlerException----1");
    }

    public static CommonResult handlerException2(BlockException exception) {
        return new CommonResult(4444,"按客戶自定义,global handlerException----2");
    }
}
