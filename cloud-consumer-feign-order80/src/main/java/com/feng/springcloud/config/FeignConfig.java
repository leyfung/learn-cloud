package com.feng.springcloud.config;

/**
 * @author jianglifeng
 * @Description TODO
 * @createTime 2021年06月03日
 */
import feign.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FeignConfig
{
    @Bean
    Logger.Level feignLoggerLevel()
    {
        return Logger.Level.FULL;
    }
}