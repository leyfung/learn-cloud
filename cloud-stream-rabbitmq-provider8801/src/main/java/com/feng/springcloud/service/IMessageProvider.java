package com.feng.springcloud.service;

/**
 * @author jianglifeng
 * @description
 * @createTime 2021年07月31日
 */
public interface IMessageProvider {
    String send();
}
